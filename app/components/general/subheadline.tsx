import React from "react";

export interface SubHeadLineProps {
  children: React.ReactNode;
}

export function SubHeadLine({ children }: SubHeadLineProps) {
  return (
    <div className="ct-subheadline flex justify-center items-center mb-8 lg:mb-24">
      <div className="ct-subheadline-deco-line w-8 h-px bg-gray-200"></div>
      <div className="ct-subheadline-label uppercase mx-4 tracking-widest text-gray-500 font-medium text-xs text-center">
        {children}
      </div>
      <div className="ct-subheadline-deco-line w-8 h-px bg-gray-200"></div>
    </div>
  );
}
