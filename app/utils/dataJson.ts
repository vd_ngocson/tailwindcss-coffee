import axios from "axios";

/**
 * Get Data Json
 */

export const getDataJson = async () => {
  /* App component ko hỗ trợ hàm getStaticProps next.js 13 */
  // const domain = process.env.DOMAIN;
  const api = `https://tailwindcss-coffee-wine.vercel.app/api/data-json`;
  // console.log(domain);
  const res = await fetch(api);
  // handle errors
  if (!res.ok) {
    throw new Error("Failed to fetch data");
  }
  //   console.log(res.json());
  return res.json();
};
