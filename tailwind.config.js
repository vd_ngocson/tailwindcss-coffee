/** @type {import('tailwindcss').Config} */

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        Karla: ['Karla', 'Sans-Serif']
      },
      // backgroundImage: {
      //   'slider-bg': 'url("./app/assets/images/slider.jpeg")'
      // }
      colors: {
        'coffee': {
          50: '#E8D6D0',
          200: '#C89F94',
          400: '#A25F4B',
          600: '#744838',
        }
      },
      keyframes: {
        slideDown: {
          '0%': {transform: 'translateY(-50%)'},
          '100%': {transform: 'translateY(0)'},
        },
        fadeIn: {
          form: {opacity: 0},
          to: {opacity: 1},
        }
      },
      animation: {
        slideDown: 'slideDown .5s ease-in-out',
        fadeIn: 'fadeIn 5s ease-in-out'
      }
    },
  },
  plugins: [],
}
