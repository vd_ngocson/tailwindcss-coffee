import React from "react";
import FormSpree from "../formspree";
import { SubHeadLine } from "../general";

export interface SubscribeProps {}

export default function Subscribe(props: SubscribeProps) {
  return (
    <div className="subscribe w-full bg-[#1d1f2e] px-24 py-20">
      <div className="subscribe-form">
        <div className="ct-subheadline flex justify-center items-center">
          <div className="ct-subheadline-deco-line w-8 h-px bg-gray-200"></div>
          <div className="ct-subheadline-label uppercase mx-4 tracking-widest text-gray-500 font-medium text-xs text-center">
            SIGN UP AND GET FREE COFFEE BAGS
          </div>
          <div className="ct-subheadline-deco-line w-8 h-px bg-gray-200"></div>
        </div>
        <div className="subscribe-headline mt-4 mb-6 text-center text-white text-4xl">
          Coffee Updates
        </div>
        <div className="form-spree flex justify-center">
          <FormSpree />
        </div>
      </div>
    </div>
  );
}
