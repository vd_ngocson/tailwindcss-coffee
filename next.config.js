/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    images: {
        domains: [
        ],
    },
    async rewrites() {
        return [
            {
                source: "/api/:path*",
                destination: "https://apicrawler.tinmoi24.vn/v1/training/:path*"
            },

        ]
    } 
};

module.exports = nextConfig;
