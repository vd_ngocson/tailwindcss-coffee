import { Inter } from "next/font/google";
// import "@/app/globals.css";

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "test",
  description: "Relaxing!123",
};

export default function TestLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={`${inter.className} overflow-x-hidden`}>
        test{children}
      </body>
    </html>
  );
}
