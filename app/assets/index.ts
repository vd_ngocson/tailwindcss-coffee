const images = {
  cart: require("@/app/assets/icons/cart.svg").default,
  user: require("@/app/assets/icons/user.png").default,
  menu: require("@/app/assets/icons/menu.svg").default,
  mainLogo: require("@/app/assets/logo/logo.png").default,
  slider: require("@/app/assets/images/slider.jpeg").default,
};

export default images;
