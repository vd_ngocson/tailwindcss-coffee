import React from "react";

export interface MagazineProps {}

export default function Magazine(props: MagazineProps) {
  return (
    <div className="coffee-magazine w-full lg:w-[90%] xl:w-[75%] mx-auto mb-24">
      <div className="flex flex-col md:flex-row justify-center items-center gap-5">
        <div className="ct-magazine-content basis-1/2 ml:8 xl:ml-14 text-center order-2 md:order-1">
          <div className="mb-5">
            <div className="uppercase text-xs tracking-widest text-gray-500 font-medium mt-9 md:mt-0">
              PREMIUM OFFER
            </div>
            <div className="text-[36px] leading-[50px] my-3">
              Get our Coffee Magazine
            </div>
            <div className="text-[16px] leading-7 text-gray-500">
              The most versatile furniture system ever created. Designed to fit
              your life.
            </div>
          </div>

          <span className="inline-block uppercase py-[18px] px-6 bg-[#1d1f2e] text-xs font-bold text-[#fff] cursor-pointer">
            Start Shopping
          </span>
        </div>
        <div className="ct-magazine-images basis-1/2 flex flex-col md:flex-row gap-5 w-full h-full order-1 md:order-2">
          <div className="ct-magazine-image h-[280px] md:basis-2/3 bg-[url('https://cdntm.24hstatic.com/ul/2023/6/15/9/0aaeec3c7ca236710c0c48a2edd5b237.jpg')] bg-cover bg-center bg-no-repeat"></div>

          <div className="ct-magazine-small-images basis-1/3 flex flex-row md:flex-col gap-5">
            <div className="ct-magazine-small-image-1 h-[130px] basis-1/2 bg-[url('https://cdntm.24hstatic.com/ul/2023/6/15/9/46a2f5df040eee5c8fc8a1d71da77225.jpg')] bg-cover bg-center bg-no-repeat"></div>
            <div className="ct-magazine-small-image-2 h-[130px] basis-1/2 bg-[url('https://cdntm.24hstatic.com/ul/2023/6/15/9/adb6dd4978fc81b24c17a5246539d660.jpg')] bg-cover bg-center bg-no-repeat"></div>
          </div>
        </div>
      </div>
    </div>
  );
}
