import firebase from "firebase/compat/app";
import "firebase/compat/auth";

const firebaseConfig = {
  apiKey: "AIzaSyB5UeYHTn86cikQoDvukwMs9-0-VPkcME0",
  authDomain: "coffee-fbc30.firebaseapp.com",
  projectId: "coffee-fbc30",
  storageBucket: "coffee-fbc30.appspot.com",
  messagingSenderId: "367194989872",
  appId: "1:367194989872:web:b97634adf4c4599e1fd4f5",
  measurementId: "G-T553J5618F",
};

firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
const appleAuthProvider = new firebase.auth.OAuthProvider("apple.com");

export { auth, googleAuthProvider, facebookAuthProvider, appleAuthProvider };
