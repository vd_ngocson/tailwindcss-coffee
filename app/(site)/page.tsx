"use client";

import { GetStaticProps, GetStaticPropsContext } from "next";
import Image from "next/image";

import Header from "../components/header";
import Magazine from "../components/magazine";
import Product from "../components/product";
import Slider from "../components/slider";
import Story from "../components/story";
import Article from "../components/article";
import { Parallak, SubHeadLine } from "@/app/components/general"; // --> bỏ export default đi
import Subscribe from "../components/subscribe";
// import Parallak from "../components/general/parallak"; /* --> thêm export default vào */
// import SubHeadLine from "@/app/components/general/subheadline"; /* --> thêm export default vào */

import { getDataJson } from "../utils";

// async function getDataArticles() {
//   /* App component ko hỗ trợ hàm getStaticProps next.js 13 */
//   const res = await fetch("https://apicrawler.tinmoi24.vn/v1/training/date");
//   // handle errors
//   if (!res.ok) {
//     throw new Error("Failed to fetch data");
//   }

//   return res.json();
// }

export interface HomePageProps {}

export default async function Home(props: HomePageProps) {
  const data = await getDataJson();

  const coffee = data.coffee;
  coffee.sort((a: any, b: any) => (a.id > b.id ? 1 : -1));

  const articlesData = data.articles;
  articlesData.sort((a: any, b: any) => (a.id > b.id ? 1 : -1));
  // console.log(data);

  return (
    <div className="content-wrapper font-Karla max-w-screen-2xl text-base mx-auto px-4 md:px-8">
      <Header />
      <Slider />
      <Story />
      <SubHeadLine>Featured Mugs</SubHeadLine>

      {/* Featured Mugs */}
      <div className="featured-mugs w-full lg:w-[90%] xl:w-[75%] mx-auto mb-24">
        <div className="grid grid-cols-1 lg:grid-cols-2 gap-8 lg:gap-4">
          <Product
            image={
              "https://cdntm.24hstatic.com/ul/2023/6/12/14/c9fe51ddad968f4b67df0ebe55746555.jpeg"
            }
            title={"Pink Premium Ceramic"}
            price={99}
            discount={0}
          />
          <Product
            image={
              "https://cdntm.24hstatic.com/ul/2023/6/12/14/5906b667429cb3afaf319390b1a71266.jpeg"
            }
            title={"Golden Designers Mug"}
            price={69}
            discount={50}
          />
        </div>
      </div>

      <SubHeadLine>More Products</SubHeadLine>

      {/* More products */}
      <div className="featured-mugs w-full lg:w-[90%] xl:w-[75%] mx-auto mb-24">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 md:gap-4">
          {coffee
            ? coffee.map((item: any) => (
                <Product
                  key={item.id}
                  image={item.image}
                  title={item.title}
                  price={item.price}
                  discount={item.discount}
                />
              ))
            : null}
        </div>
      </div>

      <SubHeadLine>BUY 2 MUGS AND GET A COFFEE MAGAZINE FREE</SubHeadLine>

      <Magazine />
      <Parallak />

      <SubHeadLine>BEHIND THE MUGS, LIFESTYLE STORIES</SubHeadLine>

      {/* Lifestyle Story*/}
      <div className="lifestyle-story w-full lg:w-[90%] xl:w-[75%] mx-auto mb-24">
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-8 md:gap-4">
          {articlesData
            ? articlesData.map((item: any) => (
                <Article
                  key={item.id}
                  image={item.image}
                  title={item.title}
                  desc={item.desc}
                  date={item.date}
                />
              ))
            : null}
        </div>
      </div>

      <Subscribe />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
