"use client";
import * as React from "react";
import { useForm, ValidationError } from "@formspree/react";

export interface FormSpreeProps {}

export default function FormSpree(props: FormSpreeProps) {
  //   const [state, handleSubmit] = useForm("1");

  //   if (state.succeeded) {
  //     return <p>Thanks for your submission!</p>;
  //   }

  const handleSubmit = (event: any) => {
    event.preventDefault();

    console.log(event.target.email.value);
  };
  return (
    <form onSubmit={handleSubmit}>
      <input
        type="email"
        name="email"
        placeholder="please enter your email!"
        className="mr-[10px] mb-0 py-[18px] px-6 w-[350px] h-[54px] text-left duration-300 border-[1px] border-solid border-[#2f303e] bg-transparent focus-visible:outline-0 hover:border-[#ffffff80] text-[#fff] text-[12px] leading-3 font-bold tracking-[2px] uppercase"
      />
      <button
        className="text-center uppercase tracking-[2px] leading-[18px] py-[18px] px-6 text-[12px] font-semibold cursor-pointer hover:bg-opacity-90 bg-white text-gray-900 opacity-80 lg:opacity-100"
        type="submit"
      >
        Subscribe
      </button>
    </form>
  );
}
