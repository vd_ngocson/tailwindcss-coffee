import React from "react";

export interface ProductProps {
  image: string;
  title: string;
  price: number;
  discount?: number;
}

export default function Product({
  image,
  title,
  price,
  discount,
}: ProductProps) {
  return (
    <div className="ct-product-card border border-slate-200">
      <div
        className="h-[400px] bg-cover bg-center bg-no-repeat"
        style={{
          backgroundImage: `url(${image})`,
        }}
      >
        <div className="w-full h-full hover:bg-gray-900 hover:bg-opacity-10 hover:transition-all hover:ease-in-out hover:duration-300 relative group">
          {discount && discount > 0 ? (
            <div className="absolute w-[100px] bg-coffee-200 py-1 px-4 top-3 right-3 text-center font-semibold text-white">
              On sale.
            </div>
          ) : null}
          {/* left-1/2 => (Sang trái 50% chiều rộng div cha) -translate-x-1/2 => (Sang trái -50% chiều rộng div con) -------> [ left: 50%; transform: translate(-50%, 0) ] */}

          <div className="absolute bg-white text-gray-900 w-11/12 bottom-4 left-1/2 -translate-x-1/2 opacity-80 lg:opacity-100 lg:hidden group-hover:block group-hover:animate-fadeIn ct-button">
            Explore Mug
          </div>
        </div>
      </div>

      <div className="text-center my-8">
        <a href="#">
          <div className="text-xl mb-3 hover:text-coffee-400 duration-300 px-2">
            {title}
          </div>
        </a>
        <div className="">
          {discount && discount > 0 ? (
            <span className="text-[20px] text-coffee-400">${discount}.00</span>
          ) : null}
          <span
            className={`ml-2 text-[16px] text-gray-400 ${
              discount && discount > 0 ? "line-through" : ""
            }`}
          >
            $ {price}.00 USD
          </span>
        </div>
      </div>
    </div>
  );
}
