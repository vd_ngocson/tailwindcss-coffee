/* eslint-disable @next/next/no-img-element */
import React from "react";
import images from "@/app/assets";
import Image from "next/image";
import Link from "next/link";

export interface HeaderProps {}

export default function Header(props: HeaderProps) {
  return (
    <header className="py-6 lg:mx-10">
      <nav className="flex flex-row justify-between">
        <div className="basis-2/8 logo text-center text-xl font-semibold cursor-pointer">
          CoffeeStyle.
        </div>
        <ul className="basis-4/8 hidden lg:flex lg:items-center lg:justify-end lg:gap-7 uppercase text-sm text-gray-500 font-medium">
          <li className="ct-top-menu-item">
            <a href="#">Home</a>
          </li>
          <li className="ct-top-menu-item">
            <a href="#">Products</a>
          </li>
          <li className="ct-top-menu-item">
            <a href="#">Blogs</a>
          </li>
          <li className="ct-top-menu-item">
            <a href="#">About</a>
          </li>
          <li className="ct-top-menu-item">
            <a href="#">Contact</a>
          </li>
          <li className="ct-top-menu-item">
            <a href="#">Styleguide</a>
          </li>
        </ul>

        <ul className="basis-6/8 lg:basis-2/8 flex justify-end lg:justify-start items-center uppercase text-sm text-gray-500 font-medium">
          <li className="ct-top-menu-item flex items-center">
            <a href="#" className="flex items-center">
              <Image className="ct-icon" src={images.cart} alt="Cart" />
              <span className="mx-1">Cart</span>
            </a>
            <span className="ct-bagde-circle bg-orange-400 text-white">10</span>
          </li>

          <li className="ct-top-menu-item flex items-center">
            <Link href="/login" className="flex items-center">
              <Image className="ct-icon" src={images.user} alt="Login" />
              <span className="mx-1">Login</span>
            </Link>
          </li>

          <li className="lg:hidden ml-6">
            <Image className="ct-icon" src={images.menu} alt="Menu" />
          </li>
        </ul>
      </nav>
    </header>
  );
}
