import React from "react";

export interface ArticleProps {
  image: string;
  title: string;
  desc: string;
  date: string;
}

export default function Article({ image, title, desc, date }: ArticleProps) {
  return (
    <div className="ct-product-card border border-slate-200">
      <div
        className="h-[300px] bg-cover bg-center bg-no-repeat"
        style={{
          backgroundImage: `url(${image})`,
        }}
      >
        <a href="">
          <div className="w-full h-full hover:bg-gray-900 hover:bg-opacity-10 hover:transition-all hover:ease-in-out duration-300 relative group">
            <div className="absolute bg-white text-gray-900 w-11/12 bottom-4 left-1/2 -translate-x-1/2 opacity-80 lg:opacity-100 lg:hidden group-hover:block group-hover:animate-fadeIn ct-button">
              Read the full Story
            </div>
          </div>
        </a>
      </div>

      <div className="text-left my-8">
        <a href="#">
          <div className="text-xl mb-3 hover:text-coffee-400 duration-300 px-2">
            {title}
          </div>
        </a>
        <div className="ml-2 mb-4 text-[16px] text-gray-500">{desc}</div>
        <div className="">
          <span
            className={`ml-2 text-[12px] text-gray-500 font-medium tracking-widest`}
          >
            {date}
          </span>
        </div>
      </div>
    </div>
  );
}
