import React from "react";

export interface ParallakProps {}

export function Parallak(props: ParallakProps) {
  return (
    <div className="parallak-section h-[340px] mb-24 bg-[url('https://cdntm.24hstatic.com/ul/2023/6/15/15/3dbc6a0eecab9f55d93d9ba5ea87c992.jpg')] bg-center bg-cover bg-fixed w-[100vw] relative left-[calc(-50vw_+_50%)]"></div>
    /* w-[100vw] relative left-[calc(-50vw_+_50%)]  -----> https://stackoverflow.com/a/24895631 */
  );
}
