import React from "react";
import images from "@/app/assets";

export interface SliderProps {}

export default function Slider(props: SliderProps) {
  return (
    <div className="slider h-[530px] bg-[url('https://cdntm.24hstatic.com/ul/2023/6/8/11/533ac0bdcf6380a90d5829ff3cf2b8c5.jpeg')] bg-cover bg-bottom bg-no-repeat">
      <div className="w-full h-full flex justify-center items-center bg-gray-900 bg-opacity-40">
        <div className="mx-16 text-white text-center">
          <div className="uppercase text-xs mb-4 tracking-[.2em]">
            BEST PLACE TO BUY DESIGN
          </div>
          <div className="font-normal text-5xl mb-4">Coffee Mugs</div>
          <div className="font-normal text-lg mb-7">
            The most versatile furniture system ever created. Designed to fit
            your life, made to move and grow.
          </div>
          <div className="flex justify-center">
            <div className="ct-button bg-white text-gray-900 w-max">
              EXPLORE OUR PRODUCTS
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
